// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    security/VOSecurityHandlerTestImpl.vsl in usp-spring-cartridge
 * MODEL CLASS: component::com.olmgroup.usp.components.course::domain::NewPersonVO
 * STEREOTYPE:  ValueObject
 */
package com.olmgroup.usp.components.course.domain.security;

import org.junit.Before;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(value = "/course-context-test.xml")
public class NewPersonVOSecurityHandlerTest extends NewPersonVOSecurityHandlerTestBase {
  @Before
  public final void handleInitializeTestSuite() {
    // TODO add any additional initialization for your test suite here
  }

  // Add additional test cases here
}
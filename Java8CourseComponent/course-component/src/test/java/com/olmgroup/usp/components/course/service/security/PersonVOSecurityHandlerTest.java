// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    security/VOSecurityHandlerTestImpl.vsl in usp-spring-cartridge
 * MODEL CLASS: component::com.olmgroup.usp.components.course::vo::PersonVO
 * STEREOTYPE:  ValueObject
 */
package com.olmgroup.usp.components.course.service.security;

import org.junit.Before;
import org.springframework.test.context.ContextConfiguration;

@ContextConfiguration(value = "/course-context-test.xml")
public class PersonVOSecurityHandlerTest extends PersonVOSecurityHandlerTestBase
{
	@Before
	public final void handleInitializeTestSuite() 
	{
		// TODO add any additional initialization for your test suite here
	}

	@Override
	protected final void handleTestGetTargetDomainObjectTypes() throws Exception
	{
		// TODO Test implementation 
		throw new UnsupportedOperationException("PersonVOSecurityHandlerTest.testGetTargetDomainObjectTypes Test Not implemented!");
	}

	@Override
	protected final void handleTestSupportsRelationalSecurityByTargetDomainObject() throws Exception {
		// TODO Test implementation 
		throw new UnsupportedOperationException("PersonVOSecurityHandlerTest.testSupportsRelationalSecurityByTargetDomainObject Test Not implemented!");
	}

	@Override
	protected final void handleTestSupportsRelationalSecurityByTargetIdAndType() throws Exception {
		// TODO Test implementation 
		throw new UnsupportedOperationException("PersonVOSecurityHandlerTest.testSupportsRelationalSecurityByTargetIdAndType Test Not implemented!");
	}

	@Override
	protected final void handleTestGetSubjectsByTargetDomainObject() throws Exception
	{
		// TODO Test implementation 
		throw new UnsupportedOperationException("PersonVOSecurityHandlerTest.testGetSubjectsByTargetDomainObject Test Not implemented!");
	}

	@Override
	protected final void handleTestGetSubjectsByTargetIdAndType() throws Exception
	{
		// TODO Test implementation 
		throw new UnsupportedOperationException("PersonVOSecurityHandlerTest.testGetSubjectsByTargetIdAndType Test Not implemented!");
	}

	// Add additional test cases here
}
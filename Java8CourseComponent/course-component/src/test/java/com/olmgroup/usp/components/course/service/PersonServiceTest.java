// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/spring/SpringServiceTestImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: component::com.olmgroup.usp.components.course::service::PersonService
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.components.course.service;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import com.olmgroup.usp.components.course.vo.PersonVO;
import com.olmgroup.usp.components.course.vo.UpdatePersonVO;

import org.junit.Before;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

/**
 * @see com.olmgroup.usp.components.course.service.PersonService
 */
@ContextConfiguration(value = "/course-context-test.xml")
@RunWith(SpringJUnit4ClassRunner.class)
public class PersonServiceTest extends PersonServiceTestBase {
  @Before
  public void handleInitializeTestSuite() {
  }

  /**
   * @see com.olmgroup.usp.components.course.service.PersonServiceTest#testFindById()
   */
  @Override
  protected void handleTestFindById() throws Exception {
    PersonService personService = getPersonService();
    PersonVO personVO = personService.findById(-1L);
    assertThat(personVO.getName(), equalTo("Jeff Buckley"));
    assertThat(personVO.getAge(), equalTo(27));

  }

  /**
   * @see com.olmgroup.usp.components.course.service.PersonServiceTest#testUpdatePerson()
   */
  @Override
  protected void handleTestUpdatePerson() throws Exception {
    PersonService personService = getPersonService();
    PersonVO personVO = personService.findById(-1L);
    assertThat(personVO.getName(), equalTo("Jeff Buckley"));
    UpdatePersonVO updatePersonVO = new UpdatePersonVO();
    updatePersonVO.setAge(28);
    updatePersonVO.setName("Jim Morrison");
    updatePersonVO.setId(-1L);
    personService.updatePerson(updatePersonVO);
    PersonVO updatedPersonVO = personService.findById(-1L);
    assertThat(updatedPersonVO.getName(), equalTo("Jim Morrison"));
    assertThat(updatedPersonVO.getAge(), equalTo(28));
  }

  /**
   * @see com.olmgroup.usp.components.course.service.PersonServiceTest#testGet()
   */
  @Override
  protected void handleTestGet() throws Exception {
    // TODO implement protected  void handleGet(@com.olmgroup.usp.facets.spring.SpelParameter("id") @com.olmgroup.usp.facets.event.EventSubject long id)
    throw new UnsupportedOperationException("com.olmgroup.usp.components.course.service.PersonServiceTest.testGet Test Not implemented!");
  }

  /**
   * @see com.olmgroup.usp.components.course.service.PersonServiceTest#testAddPerson()
   */
  @Override
  protected void handleTestAddPerson() throws Exception {
    // TODO implement protected  void handleAddPerson(@com.olmgroup.usp.facets.spring.SpelParameter("newPersonVO") @com.olmgroup.usp.facets.event.EventSubject(useReturnForPostEvent=true) com.olmgroup.usp.components.course.domain.NewPersonVO newPersonVO)
    throw new UnsupportedOperationException("com.olmgroup.usp.components.course.service.PersonServiceTest.testAddPerson Test Not implemented!");
  }

  // Add additional test cases here
}
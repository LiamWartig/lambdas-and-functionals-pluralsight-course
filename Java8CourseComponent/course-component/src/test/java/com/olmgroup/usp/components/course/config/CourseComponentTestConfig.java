package com.olmgroup.usp.components.course.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Spring configuration class used to define spring test context for the component.
 *
 * <p>TEMPLATE: ConfigurationTestBean.vsl in usp-spring-cartridge.</p>
 *
 * @author Template author: Rob D.
 */
@Configuration
@ImportResource(locations = {"classpath:META-INF/authenticationRepository.xml"})
class CourseComponentTestConfig {
}
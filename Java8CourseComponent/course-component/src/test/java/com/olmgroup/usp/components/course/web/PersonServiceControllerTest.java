// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    test/SpringServiceControllerTestImpl.vsl in andromda-spring-mvc cartridge
 * MODEL CLASS: $controller.validationName
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.components.course.web;

import org.junit.Before;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.ContextHierarchy;

/**
 * @see PersonServiceController
 */
@ContextHierarchy({
    @ContextConfiguration(value = "/course-context-test.xml"),
    @ContextConfiguration(value = "/course-servlet-context-test.xml")
})
public class PersonServiceControllerTest extends PersonServiceControllerTestBase {
  @Before
  public void handleInitializeTestSuite() {
    // TODO add any additional initialization for your test suite here
  }

  /**
   * @see PersonServiceControllerTest#testFindById()
   */
  @Override
  protected void handleTestFindById() throws Exception {
    // TODO implement protected  void handleFindById(long id, org.springframework.web.context.request.WebRequest webRequest, javax.servlet.http.HttpServletResponse servletResponse, org.springframework.ui.Model model)
    throw new UnsupportedOperationException("PersonServiceControllerTest.testFindById Test Not implemented!");
  }

  /**
   * @see PersonServiceControllerTest#testUpdatePerson()
   */
  @Override
  protected void handleTestUpdatePerson() throws Exception {
    // TODO implement protected  void handleUpdatePerson(com.olmgroup.usp.components.course.vo.UpdatePersonVO updatePersonVO, org.springframework.validation.BindingResult updatePersonVO_BindingResult, org.springframework.web.context.request.WebRequest webRequest, javax.servlet.http.HttpServletResponse servletResponse, org.springframework.ui.Model model)
    throw new UnsupportedOperationException("PersonServiceControllerTest.testUpdatePerson Test Not implemented!");
  }

  /**
   * @see PersonServiceControllerTest#testAddPerson()
   */
  @Override
  protected void handleTestAddPerson() throws Exception {
    // TODO implement protected  void handleAddPerson(com.olmgroup.usp.components.course.domain.NewPersonVO newPersonVO, org.springframework.validation.BindingResult newPersonVO_BindingResult, org.springframework.web.context.request.WebRequest webRequest, javax.servlet.http.HttpServletResponse servletResponse, org.springframework.ui.Model model)
    throw new UnsupportedOperationException("PersonServiceControllerTest.testAddPerson Test Not implemented!");
  }

  // Add additional test cases here
}

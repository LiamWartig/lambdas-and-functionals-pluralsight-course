// license-header java merge-point
package com.olmgroup.usp.components.course.domain.security;

import com.olmgroup.usp.components.course.domain.NewPersonVO;
import com.olmgroup.usp.facets.subject.vo.SubjectIdTypeVO;

import org.springframework.stereotype.Component;

import java.util.Set;

/**
 * <p>
 * Completes the implementation of the {@link NewPersonVOSecurityHandler} by implementing abstract methods defined in {@link NewPersonVOSecurityHandlerBase}.
 * </p>
 * <p>
 * This provides a {@link com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler SecurityHandler} for the {@link NewPersonVO} value object,
 * forming the basis for providing Instance Level Security.
 * </p>
 *
 * @see com.olmgroup.usp.facets.security.authorisation.voter.relational.RelationalDomainObjectHandler
 * @see com.olmgroup.usp.facets.security.authorisation.instance.DomainObjectSecurityHandler
 */
@Component("newPersonVOSecurityHandler")
public final class NewPersonVOSecurityHandlerImpl
    extends NewPersonVOSecurityHandlerBase {

  /** The serial version UID of this class. Needed for serialization. */
  private static final long serialVersionUID = 2192751838360369685L;

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(
      final NewPersonVO targetDomainObject) {
    // TODO Read instructions below and remove this on implementation.
    // Always implement this method if you return true in handleIsRelationshipCheckable() 
    //		final Set<SubjectIdTypeVO> subjectIds = new HashSet<>();
    //		return subjectIds;
    return null;
  }

  @Override
  protected Set<SubjectIdTypeVO> handleGetSubjects(final Long targetId,
      final String targetType) {
    // TODO Read instructions below and remove this on implementation.
    // Always implement this method if you return true in handleIsRelationshipCheckable() AND you have included a value in the Set returned by handleGetTargetDomainObjectTypes() 
    //		final Set<SubjectIdTypeVO> subjectIds = new HashSet<>();
    //		return subjectIds;
    return null;
  }

}
package com.olmgroup.usp.components.course.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.ImportResource;

/**
 * Spring configuration class used to define spring context for the component.
 *
 * <p>TEMPLATE: ConfigurationBean.vsl in usp-spring-cartridge.</p>
 *
 * @author Template author: Rob D.
 */
@Configuration
@ImportResource(locations = {"classpath:META-INF/components/course/ext/security/course-authorisation.xml"})
class CourseComponentConfig {
}
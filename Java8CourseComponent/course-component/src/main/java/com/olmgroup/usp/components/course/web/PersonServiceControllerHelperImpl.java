// license-header java merge-point
package com.olmgroup.usp.components.course.web;

import org.springframework.stereotype.Component;

/**
 * This class can be used to override methods of {@link PersonServiceControllerHelperBaseImpl}
 * e.g. to change how exceptions thrown by PersonServiceController are handled.
 * 
 * @see PersonServiceControllerHelperBaseImpl
 */
@Component("PersonServiceControllerHelper")
public class PersonServiceControllerHelperImpl extends PersonServiceControllerHelperBaseImpl  
{
    public PersonServiceControllerHelperImpl() { }    
}
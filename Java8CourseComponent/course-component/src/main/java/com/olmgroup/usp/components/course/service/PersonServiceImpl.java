// license-header java merge-point
/**
 * This is only generated once! It will never be overwritten.
 * You can (and have to!) safely modify it by hand.
 * TEMPLATE:    SpringServiceImpl.vsl in andromda-spring cartridge
 * MODEL CLASS: component::com.olmgroup.usp.components.course::service::PersonService
 * STEREOTYPE:  Service
 */
package com.olmgroup.usp.components.course.service;

import com.olmgroup.usp.components.course.domain.NewPersonVO;
import com.olmgroup.usp.components.course.vo.PersonVO;
import com.olmgroup.usp.components.course.vo.UpdatePersonVO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @see com.olmgroup.usp.components.course.service.PersonService
 */
@Service("personService")
public class PersonServiceImpl
    extends PersonServiceBase
{

	private static final Logger LOGGER = LoggerFactory.getLogger(PersonServiceImpl.class);

    /**    
     * {@inheritDoc}
     */
    @Override
    protected  PersonVO handleFindById(long id)
        throws Exception
    {
        // TODO implement protected  PersonVO handleFindById(long id)
        throw new UnsupportedOperationException("com.olmgroup.usp.components.course.service.PersonService.handleFindById(long id) Not implemented!");
    }

    /**    
     * {@inheritDoc}
     */
    @Override
    protected  void handleUpdatePerson(UpdatePersonVO updatePersonVO)
        throws Exception
    {
        // TODO implement protected  void handleUpdatePerson(UpdatePersonVO updatePersonVO)
        throw new UnsupportedOperationException("com.olmgroup.usp.components.course.service.PersonService.handleUpdatePerson(UpdatePersonVO updatePersonVO) Not implemented!");
    }

    /**    
     * {@inheritDoc}
     */
    @Override
    protected  PersonVO handleGet(long id)
        throws Exception
    {
        // TODO implement protected  PersonVO handleGet(long id)
        throw new UnsupportedOperationException("com.olmgroup.usp.components.course.service.PersonService.handleGet(long id) Not implemented!");
    }

    /**    
     * {@inheritDoc}
     */
    @Override
    protected  long handleAddPerson(NewPersonVO newPersonVO)
        throws Exception
    {
        // TODO implement protected  long handleAddPerson(NewPersonVO newPersonVO)
        throw new UnsupportedOperationException("com.olmgroup.usp.components.course.service.PersonService.handleAddPerson(NewPersonVO newPersonVO) Not implemented!");
    }

}